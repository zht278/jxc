package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.entity.Goods;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DamageDao {
    Integer insertDamageList(@Param("damageList") DamageList damageList);

    Integer insertDamageListGoods(@Param("damageListGoods") DamageListGoods damageListGoods);

    List<DamageList> selectDamageList(@Param("sTime") String sTime, @Param("eTime") String eTime);

    List<DamageListGoods> selectGoodsList(@Param("damageListId") Integer damageListId);
}
