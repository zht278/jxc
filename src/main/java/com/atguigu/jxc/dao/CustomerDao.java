package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Customer;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomerDao {
    List<Customer> selectCustomerList(@Param("page") int page, @Param("rows") Integer rows,@Param("customerName") String customerName);

    Integer selectTotal();

    Integer insertCustomer(@Param("customer") Customer customer);

    Integer updateCustomer(Integer customerId, Customer customer);

    Integer remove(@Param("id") int parseInt);
}
