package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.OverflowListGoods;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OverflowDao {
    Integer insertOverflow(@Param("overflowList") OverflowList overflowList);

    Integer insertOverflowListGoods(@Param("overflowListGoods") OverflowListGoods overflowListGoods);

    List<OverflowList> selectOverflowList(String sTime, String eTime);

    List<OverflowListGoods> selectGoodsListByOverflowId(@Param("overflowListId") Integer overflowListId);
}
