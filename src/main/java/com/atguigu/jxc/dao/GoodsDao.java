package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @description 商品信息
 */
@Repository
public interface GoodsDao {


    String getMaxCode();


    List<Goods> selectListByPage(@Param("page") Integer page, @Param("rows")Integer rows, @Param("codeOrName")String codeOrName, @Param("goodsTypeId")Integer goodsTypeId);


    List<Goods> selectCommodityList(@Param("page") int page, @Param("rows") Integer rows, @Param("goodsName") String goodsName, @Param("goodsTypeId") Integer goodsTypeId);

    Integer update(@Param("goods") Goods goods);

    Integer insert(@Param("goods") Goods goods);

    Integer delete(@Param("goodsId") Integer goodsId);

    Goods selectGoodsById(@Param("goodsId") Integer goodsId);

    List<Goods> selectNoInventoryQuantity(@Param("page") int page,@Param("rows") Integer rows, @Param("nameOrCode") String nameOrCode);

    Integer selectTotal(@Param("codeOrName") String codeOrName, @Param("goodsTypeId") Integer goodsTypeId,@Param("isHas")Integer isHas);

    List<Goods> selectHasInventoryQuantity(@Param("page") int page, @Param("rows") int rows, @Param("nameOrCode") String nameOrCode);

    Integer updateStock(@Param("goodsId") Integer goodsId,@Param("inventoryQuantity") Integer inventoryQuantity, @Param("purchasingPrice") double purchasingPrice);

    List<Goods> selectListAlarm();

}
