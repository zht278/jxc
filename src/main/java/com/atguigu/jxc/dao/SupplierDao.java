package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Supplier;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SupplierDao {
    List<Supplier> selectSupplierList(@Param("page") int page, @Param("rows") Integer rows,@Param("supplierName") String supplierName);

    Integer update(@Param("supplier") Supplier supplier);

    Integer save(@Param("supplier") Supplier supplier);

    Integer delete(@Param("id") int id);
}
