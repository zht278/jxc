package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.OverflowList;

public interface OverflowService {
    String saveOverflow(OverflowList overflowList, String overflowListGoodsStr);

    String selectOverflowList(String sTime, String eTime);

    String selectGoodsListByOverflowId(Integer overflowListId);
}
