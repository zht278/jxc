package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.Customer;

public interface CustomerService {
    String getCustomerList(int page, Integer rows, String customerName);

    String saveOrUpdate(Integer customerId, Customer customer);

    String delete(String ids);
}
