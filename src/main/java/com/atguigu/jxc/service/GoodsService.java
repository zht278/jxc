package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Goods;

import java.util.Map;

public interface GoodsService {


    ServiceVO getCode();


    String getListByPage(Integer page, Integer rows, String codeOrName, Integer goodsTypeId);

    String getCommodityList(int page, Integer rows, String goodsName, Integer goodsTypeId);

    String saveOrUpdate(Goods goods, Integer goodsId);

    String delete(Integer goodsId);

    String getNoInventoryQuantity(int page, Integer rows, String nameOrCode);

    String getHasInventoryQuantity(int page,int rows, String nameOrCode);

    String updateStock(Integer goodsId, Integer inventoryQuantity, double purchasingPrice);

    String deleteStock(Integer goodsId);

    String getListAlarm();
}
