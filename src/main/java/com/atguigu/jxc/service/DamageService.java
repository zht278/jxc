package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.DamageList;

public interface DamageService {
    String saveDamageList(DamageList damageList, String damageListGoodsStr);

    String selectDamageList(String sTime, String eTime);

    String selectGoodsList(Integer damageListId);
}
