package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.Supplier;

public interface SupplierService {
    String getSupplierList(Integer page, Integer rows, String supplierName);

    String saveOrUpdate(Integer supplierId, Supplier supplier);

    String delete(String ids);
}
