package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.DamageDao;
import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.service.DamageService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class DamageServiceImpl implements DamageService {
    @Autowired
    private DamageDao damageDao;
    @Autowired
    private GoodsDao goodsDao;
    @Override
    @Transactional(rollbackFor = Exception.class)
    public String saveDamageList(DamageList damageList, String damageListGoodsStr) {
        ServiceVO serviceVO;
        Integer damageList1 = damageDao.insertDamageList(damageList);
        Gson gson = new Gson();
        List<DamageListGoods> damageListGoodsList = gson.fromJson(damageListGoodsStr, new TypeToken<List<DamageListGoods>>() {}.getType());
        for (DamageListGoods damageListGoods : damageListGoodsList) {
            damageListGoods.setDamageListId(damageList.getDamageListId());
            Integer result1 = damageDao.insertDamageListGoods(damageListGoods);
            if (result1==0){
                serviceVO = new ServiceVO(500,"保存报损单失败",null);
                Gson gson1 = new GsonBuilder().create();
                String toJson = gson1.toJson(serviceVO);
                return toJson;
            }
            Goods goods = goodsDao.selectGoodsById(damageListGoods.getGoodsId());
            goods.setInventoryQuantity(goods.getInventoryQuantity()-damageListGoods.getGoodsNum());
            Integer insert = goodsDao.update(goods);
            if (insert==0){
                serviceVO = new ServiceVO(500,"保存报损单失败",null);
                Gson gson1 = new GsonBuilder().create();
                String toJson = gson1.toJson(serviceVO);
                return toJson;
            }
        }

        if (damageList1!=0){
            serviceVO = new ServiceVO(100,"请求成功",null);
        }else {
            serviceVO = new ServiceVO(500,"保存报损单失败",null);
        }
        Gson gson1 = new GsonBuilder().create();
        String toJson = gson1.toJson(serviceVO);
        return toJson;
    }

    @Override
    public String selectDamageList(String sTime, String eTime) {
        List<DamageList> damageListList = damageDao.selectDamageList(sTime,eTime);
        HashMap<String, Object> map = new HashMap<>();
        map.put("rows",damageListList);
        Gson gson = new GsonBuilder().create();
        String toJson = gson.toJson(map);
        return toJson;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public String selectGoodsList(Integer damageListId) {
        List<DamageListGoods> goodsList = damageDao.selectGoodsList(damageListId);
        Gson gson = new GsonBuilder().create();
        String toJson = gson.toJson(goodsList);
        return toJson;
    }
}
