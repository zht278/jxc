package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.UnitDao;
import com.atguigu.jxc.entity.Unit;
import com.atguigu.jxc.service.UnitService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

@Service
public class UnitServiceImpl implements UnitService {

    @Autowired
    private UnitDao unitDao;
    @Override
    public String getAllUnit() {
        List<Unit> unitList = unitDao.selectAllUnit();
        HashMap<String, Object> map = new HashMap<>();
        map.put("rows",unitList);
        Gson gson = new GsonBuilder().create();
        String toJson = gson.toJson(map);
        return toJson;
    }
}
