package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.CustomerDao;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.service.CustomerService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerDao customerDao;
    @Override
    public String getCustomerList(int page, Integer rows, String customerName) {
        List<Customer> customerList = customerDao.selectCustomerList(page,rows,customerName);
        Integer total = customerDao.selectTotal();
        HashMap<String, Object> map = new HashMap<>();
        map.put("rows",customerList);
        map.put("total",total);
        Gson gson = new GsonBuilder().create();
        String toJson = gson.toJson(map);
        return toJson;
    }

    @Override
    public String saveOrUpdate(Integer customerId, Customer customer) {
        Integer result = 0;
        if (null!=customerId){
            result = customerDao.updateCustomer(customerId,customer);
        }else {
            result = customerDao.insertCustomer(customer);
        }

        ServiceVO serviceVO;
        serviceVO = result==0?new ServiceVO(500,"添加或修改失败",null):new ServiceVO(100,"请求成功",null);
        Gson gson = new GsonBuilder().create();
        String toJson = gson.toJson(serviceVO);
        return toJson;
    }

    @Override
    public String delete(String ids) {
        String[] split = ids.split(",");
        Integer result = 0;
        if (null!=split&&split.length>0){
            for (String id : split) {
                result = customerDao.remove(Integer.parseInt(id));
            }
        }
        ServiceVO serviceVO;
        serviceVO = result==0?new ServiceVO(500,"删除客户失败",null):new ServiceVO(100,"请求成功",null);
        Gson gson = new GsonBuilder().create();
        String toJson = gson.toJson(serviceVO);
        return toJson;
    }
}
