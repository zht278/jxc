package com.atguigu.jxc.service.impl;


import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.dao.OverflowDao;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.OverflowListGoods;
import com.atguigu.jxc.service.OverflowService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.HashMap;
import java.util.List;

@Service
public class OverflowImpl implements OverflowService {
    @Autowired
    private OverflowDao overflowDao;
    @Autowired
    private GoodsDao goodsDao;

    @Override
    public String saveOverflow(OverflowList overflowList, String overflowListGoodsStr) {
        Integer result = 0;
        result = overflowDao.insertOverflow(overflowList);
        Gson gson = new Gson();
        List<OverflowListGoods> overflowListGoodsList = gson.fromJson(overflowListGoodsStr, new TypeToken<List<OverflowListGoods>>() {
        }.getType());
        ServiceVO serviceVO;
        if (!CollectionUtils.isEmpty(overflowListGoodsList)) {
            for (OverflowListGoods overflowListGoods : overflowListGoodsList) {
                overflowListGoods.setOverflowListId(overflowList.getOverflowListId());
                Integer result1 = overflowDao.insertOverflowListGoods(overflowListGoods);
                if (result1 == 0) {
                    serviceVO = new ServiceVO(500, "保存报溢单失败", null);
                    Gson gson1 = new GsonBuilder().create();
                    String toJson = gson1.toJson(serviceVO);
                    return toJson;
                }
                Goods goods = goodsDao.selectGoodsById(overflowListGoods.getGoodsId());
                goods.setInventoryQuantity(goods.getInventoryQuantity()+overflowListGoods.getGoodsNum());
                Integer update = goodsDao.update(goods);
                if (update == 0) {
                    serviceVO = new ServiceVO(500, "保存报溢单失败", null);
                    Gson gson1 = new GsonBuilder().create();
                    String toJson = gson1.toJson(serviceVO);
                    return toJson;
                }
            }
        }
        if (result == 0) {
            serviceVO = new ServiceVO(500, "保存报溢单失败", null);

        }else {
            serviceVO = new ServiceVO(100, "请求成功", null);
        }
        Gson gson1 = new GsonBuilder().create();
        String toJson = gson1.toJson(serviceVO);
        return toJson;
    }

    @Override
    public String selectOverflowList(String sTime, String eTime) {
        List<OverflowList> overflowListList = overflowDao.selectOverflowList(sTime,eTime);
        HashMap<String, Object> map = new HashMap<>();
        map.put("rows",overflowListList);
        Gson gson = new GsonBuilder().create();
        String toJson = gson.toJson(map);
        return toJson;
    }

    @Override
    public String selectGoodsListByOverflowId(Integer overflowListId) {
        List<OverflowListGoods> overflowListGoodsList = overflowDao.selectGoodsListByOverflowId(overflowListId);
        HashMap<String, Object> map = new HashMap<>();
        map.put("rows",overflowListGoodsList);
        Gson gson = new GsonBuilder().create();
        String toJson = gson.toJson(map);
        return toJson;
    }
}
