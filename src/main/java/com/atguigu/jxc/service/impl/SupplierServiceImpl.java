package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.SupplierDao;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.List;

@Service
public class SupplierServiceImpl implements SupplierService {
    @Autowired
    private SupplierDao supplierDao;
    @Override
    public String getSupplierList(Integer page, Integer rows, String supplierName) {
        List<Supplier> supplierList = supplierDao.selectSupplierList(page-1,rows,supplierName);
        HashMap<String, Object> map = new HashMap<>();
        map.put("total",supplierList.size());
        map.put("rows",supplierList);
        Gson gson = new GsonBuilder().create();
        String result = gson.toJson(map);
        return result;
    }

    @Override
    public String saveOrUpdate(Integer supplierId, Supplier supplier) {
        Integer result=0;
        if (null!=supplierId){
            supplier.setSupplierId(supplierId);
            result = supplierDao.update(supplier);
        }else{
            result = supplierDao.save(supplier);
        }
        ServiceVO serviceVO;
        serviceVO = result==0?new ServiceVO(500,"添加或更新失败",null):new ServiceVO(200,"请求成功",null);
        Gson gson = new GsonBuilder().create();
        String json = gson.toJson(serviceVO);
        return json;
    }

    @Override
    public String delete(String ids) {
        String[] idsNew = ids.split(",");
        Integer result = 0;
        if (null!=idsNew && idsNew.length>0){
            for (String id : idsNew) {
                result = supplierDao.delete(Integer.parseInt(id));
            }
        }
        ServiceVO serviceVO;
        serviceVO = result==0?new ServiceVO(500,"删除供应商失败",null):new ServiceVO(200,"请求成功",null);
        Gson gson = new GsonBuilder().create();
        String toJson = gson.toJson(serviceVO);
        return toJson;
    }
}
