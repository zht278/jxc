package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.dao.ReturnListGoodsDao;
import com.atguigu.jxc.dao.SaleListGoodsDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.Log;
import com.atguigu.jxc.entity.ReturnListGoods;
import com.atguigu.jxc.entity.SaleListGoods;
import com.atguigu.jxc.service.CustomerReturnListGoodsService;
import com.atguigu.jxc.service.GoodsService;
import com.atguigu.jxc.service.LogService;
import com.atguigu.jxc.service.SaleListGoodsService;
import com.fasterxml.jackson.databind.util.JSONPObject;
import com.google.gson.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @description
 */
@Service
public class GoodsServiceImpl implements GoodsService {

    @Autowired
    private GoodsDao goodsDao;

    @Autowired
    private SaleListGoodsDao saleListGoodsDao;

    @Autowired
    private ReturnListGoodsDao returnListGoodsDao;

    @Override
    public ServiceVO getCode() {

        // 获取当前商品最大编码
        String code = goodsDao.getMaxCode();

        // 在现有编码上加1
        Integer intCode = Integer.parseInt(code) + 1;

        // 将编码重新格式化为4位数字符串形式
        String unitCode = intCode.toString();

        for(int i = 4;i > intCode.toString().length();i--){

            unitCode = "0"+unitCode;

        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS, unitCode);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public String getListByPage(Integer page, Integer rows, String codeOrName, Integer goodsTypeId) {
        HashMap<String, Object> map = new HashMap<>();
        List<Goods> goods =  goodsDao.selectListByPage(page,rows,codeOrName,goodsTypeId);

        if (!CollectionUtils.isEmpty(goods)) {
            goods.forEach(goods1 -> {
                List<SaleListGoods> saleListGoodsList = saleListGoodsDao.selectSaleTotal(goods1.getGoodsId());

                if (!CollectionUtils.isEmpty(saleListGoodsList)) {
                    List<Integer> returnListGoodsList = returnListGoodsDao.selectReturnTotal(goods1.getGoodsId());
                    Integer sum = 0;
                    for (SaleListGoods saleListGoods : saleListGoodsList) {
                        sum += saleListGoods.getGoodsNum();
                    }
                    if (!CollectionUtils.isEmpty(returnListGoodsList)){
                        for (Integer integer : returnListGoodsList) {
                            sum -= integer;
                        }
                    }
                    goods1.setSaleTotal(sum);
                }else {
                    goods1.setSaleTotal(0);
                }
            });
        }
        Integer total = goodsDao.selectTotal(codeOrName,goodsTypeId,null);
        map.put("total",total);
        map.put("rows",goods);
        Gson gson = new GsonBuilder().create();
        String result = gson.toJson(map);
        return result;
    }

    @Override
    public String getCommodityList(int page, Integer rows, String goodsName, Integer goodsTypeId) {
        List<Goods> goodsList = goodsDao.selectCommodityList(page,rows,goodsName,goodsTypeId);
        Integer total = goodsDao.selectTotal(goodsName,goodsTypeId,null);
        HashMap<String, Object> map = new HashMap<>();
        map.put("total",total);
        map.put("rows",goodsList);
        Gson gson = new GsonBuilder().create();
        String toJson = gson.toJson(map);
        return toJson;
    }

    @Override
    public String saveOrUpdate(Goods goods, Integer goodsId) {
        Integer result = 0;
        if (null!=goodsId){
            goods.setGoodsId(goodsId);
            result = goodsDao.update(goods);
        }else {
            goods.setInventoryQuantity(0);
            goods.setLastPurchasingPrice(0);
            goods.setState(0);
            result = goodsDao.insert(goods);
        }
        ServiceVO serviceVO;
        serviceVO = result==0?new ServiceVO(500,"添加或更新失败",null):new ServiceVO(100,"请求成功",null);
        Gson gson = new GsonBuilder().create();
        String json = gson.toJson(serviceVO);
        return json;
    }

    @Override
    public String delete(Integer goodsId) {
        Goods goods = goodsDao.selectGoodsById(goodsId);
        Integer result = 0 ;
        if (null!=goods){
            if (goods.getState()!=1&&goods.getState()!=2){
                result = goodsDao.delete(goodsId);
            }
        }
        ServiceVO serviceVO;
        serviceVO = result==0?new ServiceVO(500,"删除失败",null):new ServiceVO(100,"请求成功",null);
        Gson gson = new GsonBuilder().create();
        String json = gson.toJson(serviceVO);
        return json;
    }

    @Override
    public String getNoInventoryQuantity(int page, Integer rows, String nameOrCode) {
        List<Goods> goodsList = goodsDao.selectNoInventoryQuantity(page,rows,nameOrCode);
        HashMap<String, Object> map = new HashMap<>();
        Integer total = goodsDao.selectTotal(nameOrCode,null,-1);
        map.put("total",total);
        map.put("rows",goodsList);
        Gson gson = new GsonBuilder().create();
        String toJson = gson.toJson(map);
        return toJson;
    }

    @Override
    public String getHasInventoryQuantity(int page, int rows, String nameOrCode) {
        List<Goods> goodsList = goodsDao.selectHasInventoryQuantity(page,rows,nameOrCode);
        HashMap<String, Object> map = new HashMap<>();
        Integer total = goodsDao.selectTotal(nameOrCode, null,1);
        map.put("total",total);
        map.put("rows",goodsList);
        Gson gson = new GsonBuilder().create();
        String toJson = gson.toJson(map);
        return toJson;
    }

    @Override
    public String updateStock(Integer goodsId, Integer inventoryQuantity, double purchasingPrice) {
        Integer result = goodsDao.updateStock(goodsId,inventoryQuantity,purchasingPrice);
        ServiceVO serviceVO;
        serviceVO = result==0?new ServiceVO(500,"修改库存数量或成本价失败",null):new ServiceVO(100,"请求成功",null);
        Gson gson = new GsonBuilder().create();
        String json = gson.toJson(serviceVO);
        return json;
    }

    @Override
    public String deleteStock(Integer goodsId) {
        Goods goods = goodsDao.selectGoodsById(goodsId);
        Integer result = 0;
        if (null!=goods){
            if (goods.getState()==0){
                result = goodsDao.updateStock(goodsId,0,goods.getPurchasingPrice());
            }
        }
        ServiceVO serviceVO;
        serviceVO = result==0?new ServiceVO(500,"删除库存失败",null):new ServiceVO(100,"请求成功",null);
        Gson gson = new GsonBuilder().create();
        String json = gson.toJson(serviceVO);
        return json;
    }

    @Override
    public String getListAlarm() {
        List<Goods> goodsList = goodsDao.selectListAlarm();
        HashMap<String, Object> map = new HashMap<>();
        map.put("rows",goodsList);
        Gson gson = new GsonBuilder().create();
        String toJson = gson.toJson(map);
        return toJson;
    }


}
