package com.atguigu.jxc.controller;

import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/supplier")
public class SupplierController {
    @Autowired
    private SupplierService supplierService;

    @PostMapping("/list")
    public String getSupplierList(Integer page,
                                  Integer rows,
                                  String supplierName){
        String result = supplierService.getSupplierList(page,rows,supplierName);
        return result;
    }

    @PostMapping("/save")
    public String saveOrUpdate(Integer supplierId,Supplier supplier){
        String result = supplierService.saveOrUpdate(supplierId,supplier);
        Supplier supplier1 = new Supplier();
        return result;
    }

    @PostMapping("/delete")
    public String delete(String ids){
        String result = supplierService.delete(ids);
        return result;
    }
}
