package com.atguigu.jxc.controller;

import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/customer")
public class CustomerController {
    @Autowired
    private CustomerService customerService;

    @PostMapping("/list")
    public String getCustomerList(Integer page,Integer rows,String customerName){
        String result =customerService.getCustomerList((page-1)*rows,rows,customerName);
        return result;
    }

    @PostMapping("/save")
    public String saveOrUpdate(Integer customerId, Customer customer){
        String result = customerService.saveOrUpdate(customerId,customer);
        return result;
    }

    @PostMapping("/delete")
    public String delete(String ids){
        String result = customerService.delete(ids);
        return result;
    }
}
