package com.atguigu.jxc.controller;

import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.User;
import com.atguigu.jxc.service.OverflowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;

@RestController
@RequestMapping("/overflowListGoods")
public class OverflowController {
    @Autowired
    private OverflowService overflowService;

    @PostMapping("/save")
    public String save(String overflowNumber, OverflowList overflowList, String overflowListGoodsStr, HttpSession session) {
        User user = (User) session.getAttribute("currentUser");
        overflowList.setOverflowNumber(overflowNumber);
        overflowList.setUserId(user.getUserId());
        String result = overflowService.saveOverflow(overflowList, overflowListGoodsStr);
        return result;
    }

    @PostMapping("/list")
    public String getOverflowList(String sTime, String eTime) {
        String result = overflowService.selectOverflowList(sTime,eTime);
        return result;
    }

    @PostMapping("/goodsList")
    public String getGoodsListByOverflowId(Integer overflowListId){
        String result =overflowService.selectGoodsListByOverflowId(overflowListId);
        return result;
    }
}
