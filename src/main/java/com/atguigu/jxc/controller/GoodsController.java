package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.service.GoodsService;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @description 商品信息Controller
 */

@RestController
@RequestMapping("/goods")
public class GoodsController {

    @Autowired
    private GoodsService goodsService;

    /**
     * 分页查询商品库存信息
     *
     * @param page        当前页
     * @param rows        每页显示条数
     * @param codeOrName  商品编码或名称
     * @param goodsTypeId 商品类别ID
     * @return
     */
    @PostMapping("/listInventory")
    public String listInventory(Integer page,
                                Integer rows,
                                String codeOrName,
                                Integer goodsTypeId) {
        String result = goodsService.getListByPage((page - 1) * rows, rows, codeOrName, goodsTypeId);
        return result;
    }


    /**
     * 分页查询商品信息
     *
     * @param page        当前页
     * @param rows        每页显示条数
     * @param goodsName   商品名称
     * @param goodsTypeId 商品类别ID
     * @return
     */
    @PostMapping("/list")
    public String getCommodityList(Integer page, Integer rows, String goodsName, Integer goodsTypeId) {
        String result = goodsService.getCommodityList((page - 1) * rows, rows, goodsName, goodsTypeId);
        return result;
    }


    /**
     * 生成商品编码
     *
     * @return
     */
    @RequestMapping("/getCode")
    @RequiresPermissions(value = "商品管理")
    public ServiceVO getCode() {
        return goodsService.getCode();
    }

    /**
     * 添加或修改商品信息
     *
     * @param goods 商品信息实体
     * @return
     */
    @PostMapping("/save")
    public String saveOrUpdate(Goods goods, Integer goodsId) {
        String result = goodsService.saveOrUpdate(goods, goodsId);
        return result;
    }

    /**
     * 删除商品信息
     *
     * @param goodsId 商品ID
     * @return
     */
    @PostMapping("/delete")
    public String delete(Integer goodsId) {
        String result = goodsService.delete(goodsId);
        return result;
    }

    /**
     * 分页查询无库存商品信息
     *
     * @param page       当前页
     * @param rows       每页显示条数
     * @param nameOrCode 商品名称或商品编码
     * @return
     */
    @PostMapping("/getNoInventoryQuantity")
    public String getNoInventoryQuantity(Integer page, Integer rows, String nameOrCode) {
        String result = goodsService.getNoInventoryQuantity((page - 1) * rows, rows, nameOrCode);
        return result;
    }


    /**
     * 分页查询有库存商品信息
     *
     * @param page       当前页
     * @param rows       每页显示条数
     * @param nameOrCode 商品名称或商品编码
     * @return
     */
    @PostMapping("/getHasInventoryQuantity")
    public String getHasInventoryQuantity(Integer page, Integer rows, String nameOrCode) {
        String result = goodsService.getHasInventoryQuantity((page - 1) * rows, rows, nameOrCode);
        return result;
    }


    /**
     * 添加商品期初库存
     *
     * @param goodsId           商品ID
     * @param inventoryQuantity 库存
     * @param purchasingPrice   成本价
     * @return
     */
    @PostMapping("/saveStock")
    public String saveStock(Integer goodsId, Integer inventoryQuantity, double purchasingPrice) {
        String result = goodsService.updateStock(goodsId,inventoryQuantity,purchasingPrice);
        return result;
    }

    /**
     * 删除商品库存
     * @param goodsId 商品ID
     * @return
     */
    @PostMapping("/deleteStock")
    public String deleteStock(Integer goodsId){
        String result = goodsService.deleteStock(goodsId);
        return result;
    }

    /**
     * 查询库存报警商品信息
     * @return
     */
    @PostMapping("/listAlarm")
    public String listAlarm(){
        String result = goodsService.getListAlarm();
        return result;
    }

}
