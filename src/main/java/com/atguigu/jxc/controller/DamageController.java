package com.atguigu.jxc.controller;

import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.User;
import com.atguigu.jxc.service.DamageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;

@RestController
@RequestMapping("/damageListGoods")
public class DamageController {

    @Autowired
    private DamageService damageService;

    @PostMapping("/save")
    public String saveDamageList(String damageNumber, DamageList damageList, String damageListGoodsStr, HttpSession session) {
        User user = (User) session.getAttribute("currentUser");
        damageList.setDamageNumber(damageNumber);
        damageList.setUserId(user.getUserId());
        String result = damageService.saveDamageList(damageList, damageListGoodsStr);
        return result;
    }

    @PostMapping("/list")
    public String getDamageList(String sTime, String eTime) {
        String result = damageService.selectDamageList(sTime,eTime);
        return result;
    }

    @PostMapping("/goodsList")
    public String getGoodsList(Integer damageListId){
        String result = damageService.selectGoodsList(damageListId);
        return result;
    }
}
